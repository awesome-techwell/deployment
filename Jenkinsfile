def label = "worker-${UUID.randomUUID().toString()}"
def DOCKER_REPO = "localhost:32002"
def NEXUS_REPO_HOST
def NEXUS_REPO_PORT

podTemplate(
        label: label, containers: [
            containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
            containerTemplate(name: 'helm', image: 'lachlanevenson/k8s-helm:v2.14.3', command: 'cat', ttyEnabled: true),
            containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:latest', command: 'cat', ttyEnabled: true),
            containerTemplate(name: 'testcontainer', image: 'localhost:32002/testcontainer', ports: [portMapping(name: 'deploy-zap', containerPort: 32269)],  command: 'cat', ttyEnabled: true )
        ],
        volumes: [
                hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
        ]) {
    node(label) {

        sh "env"

        NEXUS_REPO_HOST = "${env.SONATYPE_NEXUS_SERVICE_SERVICE_HOST}"
        NEXUS_REPO_PORT = env.SONATYPE_NEXUS_SERVICE_SERVICE_PORT_HTTP

        stage('Checkout deployment repo') {
            echo "checking out deployment"
            checkout scm
        }

        stage('Deploy Services') {
            /*
            parallel AuthService: {
                build 'auth-service'
            },

                    UserService: {
                        build 'user-service'
                    },
                    CatalogService: {
                        build 'catalog-service'
                    },
                    APIGateway: {
                        build 'gateway'
                    },
                    UI: {
                        build 'angular-ui'
                    }
            */
        }

        stage('Deploy to QA') {
            container("docker") {
                docker.withRegistry("http://${DOCKER_REPO}", 'nexus') {
                    container("helm") {
                        withCredentials([

                                usernamePassword(
                                        credentialsId: 'nexus',
                                        usernameVariable: 'nexususername',
                                        passwordVariable: 'nexuspassword'
                                )
                        ]) {
                            stage('Package helm') {
                                dir("umbrella") {
                                    sh "ls"
                                    sh "helm init --client-only"
                                    sh "helm package codeverosui"
                                    sh "ls"
                                    sh "apk add curl"
                                    sh "curl -u ${nexususername}:${nexuspassword} -v http://${NEXUS_REPO_HOST}:${NEXUS_REPO_PORT}/repository/helm/ --upload-file codeverosui-0.1.0.tgz"
                                }
                            }
                            stage('Deploy helm') {
                                dir("umbrella") {
                                    echo "deploy package"
                                    sh "ls"
                                    sh "helm upgrade --install --wait codeverosui-release codeverosui-0.1.0.tgz --set image.repository=${DOCKER_REPO}/codeverosui/codeverosui --namespace codeveros"
                                }
                            }
                        }
                    }
                    container('kubectl') {

                        APP_IP = sh(
                                script: 'kubectl get svc codeverosui --namespace=codeveros  -o jsonpath="{.spec.clusterIP}"',
                                returnStdout: true
                        ).trim()
                        APP_URL = "http://${APP_IP}:80"
                        echo "APP_URL=$APP_URL"
                    }
                }
            }
        }

        stage("Post-Deployment Checks") {
            parallel(
                    SmokeTest: {
                        try {
                            echo 'Perform smoke test for system viability'
                            sh "wget ${APP_URL}/login &>/dev/null"
                        } catch (err) {
                            echo "Error found running the smoke test."
                            currentBuild.result = "FAILURE"
                            throw err
                        }
                    },
                    IntegrationTest: {
                        echo 'Run integration tests'
                        sh("${WORKSPACE}/working.sh")
                    },
                    FunctionalTest: {
                        echo 'Run functional tests'
                        container('kubectl') {
                            writeFile file: "service.yaml", text: '''apiVersion: v1
kind: Service
metadata:
  name: deploy-zap
  namespace: default
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: http
    nodePort: 32269
    port: 32269
    protocol: TCP
    targetPort: 32269
  sessionAffinity: None
  type: NodePort
  selector:
    app: test
'''
                            sh "kubectl apply -f service.yaml; sleep 5;"
                            sh "wget https://github.com/zaproxy/zaproxy/releases/download/v2.8.0/ZAP_2.8.0_Linux.tar.gz; tar xzvf ZAP_2.8.0_Linux.tar.gz; mv ZAP_2.8.0 zaproxy"
                            container('testcontainer') {
                                sh "echo 127.0.0.1 deploy-zap localhost >> /etc/hosts"
                                startZap(host: "deploy-zap", port: 32269, timeout: 9000, zapHome: "${WORKSPACE}/zaproxy")
                                try {
                                    //ideally, the angular-ui app should be publishing the tests somewhere (nexus), so that other modules can use it
                                    //we're gonna cheat, and just check them out, and run them
                                    dir('ui-tests') {
                                        checkout([
                                                $class           : 'GitSCM',
                                                branches         : [[name: "*/demo/marketing1.0"]],
                                                userRemoteConfigs: [[
                                                                            credentialsId: 'angularui-gitlab-deploy-key',
                                                                            url: 'git@gitlab.com:coveros/codeveros/codeveros-angular-ui.git'
                                                                    ]]
                                        ])
                                        try {
                                            sh("mvn clean verify -Dbrowser=chrome -Dheadless=true -DappURL=${APP_URL}/ -Dproxy=deploy-zap:32269")
                                        } catch (e) {
                                            throw e
                                        } finally {
                                            junit 'target/failsafe-reports/TEST-*.xml'
                                            publishHTML([
                                                    allowMissing         : false,
                                                    alwaysLinkToLastBuild: true,
                                                    keepAll              : true,
                                                    reportDir            : 'target/failsafe-reports',
                                                    reportFiles          : 'report.html',
                                                    reportName           : 'Chrome Report'
                                            ])
                                        }
                                    }
                                } catch (e) {
                                    throw e
                                } finally {
                                    sh 'mkdir -p zap-proxy; sleep 30;'
                                    sh "curl -o zap-proxy/report.html http://deploy-zap:32269/OTHER/core/other/htmlreport"
                                    sh "curl -o zap-proxy/report.xml http://deploy-zap:32269/OTHER/core/other/xmlreport"
                                    publishHTML([
                                            allowMissing         : false,
                                            alwaysLinkToLastBuild: true,
                                            keepAll              : true,
                                            reportDir            : 'zap-proxy',
                                            reportFiles          : 'report.html',
                                            reportName           : 'ZAP Proxy Report'
                                    ])
                                }
                            }
                        }
                    }
            )
        }
        stage("Non-Functional Tests") {
            parallel PerformanceTest: {
                echo 'Run performance test suite'
                sh("./working.sh")
            },

                    LoadTest: {
                        echo 'Run load testing scenarios'
                        sh("./working.sh")
                    },
                    Acessibility: {
                        echo 'Run Section 508 Compliance Scan'
                        sh("./working.sh")
                    },
                    DynamicSecurityTest: {
                        echo 'Run Dynamic Security Tests'
                        sh("./working.sh")
                    }
        }

        stage("Deploy to Prod") {
            echo 'Deploying application to production'
            sh("./working.sh")
        }
        stage("Post-Deployment Checks") {
            parallel SmokeTest: {
                echo 'Perform smoke test for system viability'
                sh("./working.sh")
            }
        }
    }
}
